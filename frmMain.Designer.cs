﻿namespace Budicek
{
  partial class frmMain
  {
    /// <summary>
    /// Required designer variable.
    /// </summary>
    private System.ComponentModel.IContainer components = null;

    /// <summary>
    /// Clean up any resources being used.
    /// </summary>
    /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
    protected override void Dispose(bool disposing)
    {
      if (disposing && (components != null))
      {
        components.Dispose();
      }
      base.Dispose(disposing);
    }

    #region Windows Form Designer generated code

    /// <summary>
    /// Required method for Designer support - do not modify
    /// the contents of this method with the code editor.
    /// </summary>
    private void InitializeComponent()
    {
      this.components = new System.ComponentModel.Container();
      System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(frmMain));
      this.numDelka = new System.Windows.Forms.NumericUpDown();
      this.label1 = new System.Windows.Forms.Label();
      this.btnStart = new System.Windows.Forms.Button();
      this.btnStop = new System.Windows.Forms.Button();
      this.groupBox1 = new System.Windows.Forms.GroupBox();
      this.prgMain = new System.Windows.Forms.ProgressBar();
      this.lblProgress = new System.Windows.Forms.Label();
      this.timMain = new System.Windows.Forms.Timer(this.components);
      this.pictureBox1 = new System.Windows.Forms.PictureBox();
      this.notifyIcon = new System.Windows.Forms.NotifyIcon(this.components);
      this.btnKonec = new System.Windows.Forms.Button();
      ((System.ComponentModel.ISupportInitialize)(this.numDelka)).BeginInit();
      this.groupBox1.SuspendLayout();
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
      this.SuspendLayout();
      // 
      // numDelka
      // 
      this.numDelka.Location = new System.Drawing.Point(159, 6);
      this.numDelka.Name = "numDelka";
      this.numDelka.Size = new System.Drawing.Size(55, 20);
      this.numDelka.TabIndex = 0;
      // 
      // label1
      // 
      this.label1.AutoSize = true;
      this.label1.Location = new System.Drawing.Point(87, 9);
      this.label1.Name = "label1";
      this.label1.Size = new System.Drawing.Size(66, 13);
      this.label1.TabIndex = 1;
      this.label1.Text = "Počet minut:";
      // 
      // btnStart
      // 
      this.btnStart.Location = new System.Drawing.Point(327, 6);
      this.btnStart.Name = "btnStart";
      this.btnStart.Size = new System.Drawing.Size(75, 20);
      this.btnStart.TabIndex = 2;
      this.btnStart.Text = "Start";
      this.btnStart.UseVisualStyleBackColor = true;
      this.btnStart.Click += new System.EventHandler(this.btnStart_Click);
      // 
      // btnStop
      // 
      this.btnStop.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnStop.Location = new System.Drawing.Point(327, 32);
      this.btnStop.Name = "btnStop";
      this.btnStop.Size = new System.Drawing.Size(75, 23);
      this.btnStop.TabIndex = 3;
      this.btnStop.Text = "Stop";
      this.btnStop.UseVisualStyleBackColor = true;
      this.btnStop.Click += new System.EventHandler(this.btnStop_Click);
      // 
      // groupBox1
      // 
      this.groupBox1.Controls.Add(this.prgMain);
      this.groupBox1.Controls.Add(this.lblProgress);
      this.groupBox1.Location = new System.Drawing.Point(86, 25);
      this.groupBox1.Name = "groupBox1";
      this.groupBox1.Size = new System.Drawing.Size(235, 56);
      this.groupBox1.TabIndex = 4;
      this.groupBox1.TabStop = false;
      // 
      // prgMain
      // 
      this.prgMain.Location = new System.Drawing.Point(4, 19);
      this.prgMain.Name = "prgMain";
      this.prgMain.Size = new System.Drawing.Size(225, 16);
      this.prgMain.Step = 1;
      this.prgMain.TabIndex = 1;
      // 
      // lblProgress
      // 
      this.lblProgress.Location = new System.Drawing.Point(66, 38);
      this.lblProgress.Name = "lblProgress";
      this.lblProgress.Size = new System.Drawing.Size(103, 13);
      this.lblProgress.TabIndex = 0;
      this.lblProgress.Text = "-";
      this.lblProgress.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
      // 
      // timMain
      // 
      this.timMain.Interval = 1000;
      this.timMain.Tick += new System.EventHandler(this.timMain_Tick);
      // 
      // pictureBox1
      // 
      this.pictureBox1.Image = ((System.Drawing.Image)(resources.GetObject("pictureBox1.Image")));
      this.pictureBox1.Location = new System.Drawing.Point(3, 10);
      this.pictureBox1.Name = "pictureBox1";
      this.pictureBox1.Size = new System.Drawing.Size(77, 71);
      this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
      this.pictureBox1.TabIndex = 5;
      this.pictureBox1.TabStop = false;
      // 
      // notifyIcon
      // 
      this.notifyIcon.BalloonTipText = "Budíček";
      this.notifyIcon.Icon = ((System.Drawing.Icon)(resources.GetObject("notifyIcon.Icon")));
      this.notifyIcon.Visible = true;
      this.notifyIcon.BalloonTipClosed += new System.EventHandler(this.notifyIcon_BalloonTipClosed);
      this.notifyIcon.BalloonTipShown += new System.EventHandler(this.notifyIcon_BalloonTipShown);
      this.notifyIcon.Click += new System.EventHandler(this.notifyIcon_Click);
      this.notifyIcon.DoubleClick += new System.EventHandler(this.notifyIcon_DoubleClick);
      this.notifyIcon.MouseMove += new System.Windows.Forms.MouseEventHandler(this.notifyIcon_MouseMove);
      // 
      // btnKonec
      // 
      this.btnKonec.DialogResult = System.Windows.Forms.DialogResult.Cancel;
      this.btnKonec.Location = new System.Drawing.Point(327, 61);
      this.btnKonec.Name = "btnKonec";
      this.btnKonec.Size = new System.Drawing.Size(75, 23);
      this.btnKonec.TabIndex = 6;
      this.btnKonec.Text = "Konec";
      this.btnKonec.UseVisualStyleBackColor = true;
      this.btnKonec.Click += new System.EventHandler(this.btnKonec_Click);
      // 
      // frmMain
      // 
      this.AcceptButton = this.btnStart;
      this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
      this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
      this.CancelButton = this.btnStop;
      this.ClientSize = new System.Drawing.Size(411, 93);
      this.Controls.Add(this.btnKonec);
      this.Controls.Add(this.pictureBox1);
      this.Controls.Add(this.groupBox1);
      this.Controls.Add(this.btnStop);
      this.Controls.Add(this.btnStart);
      this.Controls.Add(this.label1);
      this.Controls.Add(this.numDelka);
      this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.FixedSingle;
      this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
      this.MaximizeBox = false;
      this.Name = "frmMain";
      this.Text = "Budíček";
      this.FormClosing += new System.Windows.Forms.FormClosingEventHandler(this.frmMain_FormClosing);
      this.Load += new System.EventHandler(this.frmMain_Load);
      ((System.ComponentModel.ISupportInitialize)(this.numDelka)).EndInit();
      this.groupBox1.ResumeLayout(false);
      ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
      this.ResumeLayout(false);
      this.PerformLayout();

    }

    #endregion

    private System.Windows.Forms.NumericUpDown numDelka;
    private System.Windows.Forms.Label label1;
    private System.Windows.Forms.Button btnStart;
    private System.Windows.Forms.Button btnStop;
    private System.Windows.Forms.GroupBox groupBox1;
    private System.Windows.Forms.ProgressBar prgMain;
    private System.Windows.Forms.Label lblProgress;
    private System.Windows.Forms.Timer timMain;
    private System.Windows.Forms.PictureBox pictureBox1;
    private System.Windows.Forms.NotifyIcon notifyIcon;
    private System.Windows.Forms.Button btnKonec;
  }
}

