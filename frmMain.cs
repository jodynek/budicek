﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.IO;
using System.Text;
using System.Threading.Tasks;
using System.Media;
using System.Windows.Forms;
using Budicek.Properties;

namespace Budicek
{
  public partial class frmMain : Form
  {
    DateTime endTime;
    int iPocetSec = 0;
    private bool canClose = false;
    private bool bBaloonVisible = false;

    public frmMain()
    {
      InitializeComponent();
    }

    private void frmMain_Load(object sender, EventArgs e)
    {
      btnStop.Enabled = false;
      Settings.Default.Reload();
      numDelka.Value = Settings.Default.TimerValue;
    }

    private void btnStart_Click(object sender, EventArgs e)
    {
      btnStart.Enabled = false;
      btnStop.Enabled = true;
      int iSecs = (int)numDelka.Value * 60;
      prgMain.Maximum = iSecs;
      iPocetSec = iSecs;
      endTime = DateTime.Now;
      endTime = endTime.AddSeconds(iSecs);
      this.WindowState = FormWindowState.Minimized;
      this.ShowInTaskbar = false;
      timMain.Start();
    }

    private void timMain_Tick(object sender, EventArgs e)
    {
      if (endTime <= DateTime.Now)
      {
        timMain.Stop();
        if (this.WindowState == FormWindowState.Minimized)
          this.WindowState = FormWindowState.Normal;
        this.Text = "Budíček";
        notifyIcon.BalloonTipText = this.Text;
        notifyIcon.BalloonTipTitle = this.Text;
        btnStart.Enabled = true;
        btnStop.Enabled = false;
        lblProgress.Text = "-";
        prgMain.Value = 0;
        // alarm
        Stream str = Properties.Resources.SIREN;
        SoundPlayer snd = new SoundPlayer(str);
#if !DEBUG
        snd.Play();
#endif
        MessageBox.Show("Už je čas!", "Informace", MessageBoxButtons.OK, MessageBoxIcon.Information);
        snd.Stop();
      }
      else
      {
        prgMain.PerformStep();
        iPocetSec--;
        lblProgress.Text = "Zbývá " + iPocetSec.ToString() + " sekund";
        this.Text = "Budíček - zbývá " + iPocetSec.ToString() + " sekund";
        notifyIcon.BalloonTipText = this.Text;
        notifyIcon.BalloonTipTitle = this.Text;
      }
    }

    private void btnStop_Click(object sender, EventArgs e)
    {
      timMain.Stop();
      btnStart.Enabled = true;
      btnStop.Enabled = false;
      lblProgress.Text = "-";
      this.Text = "Budíček";
    }

    private void frmMain_FormClosing(object sender, FormClosingEventArgs e)
    {
      Settings.Default.TimerValue = (int)numDelka.Value;
      Settings.Default.Save();
      if (canClose)
        e.Cancel = false;
      else
      {
        this.WindowState = FormWindowState.Minimized;
        this.ShowInTaskbar = false;
        e.Cancel = true;
      }
    }

    private void btnKonec_Click(object sender, EventArgs e)
    {
      canClose = true;
      Close();
    }

    private void notifyIcon_DoubleClick(object sender, EventArgs e)
    {
      if (this.WindowState == FormWindowState.Normal)
      {
        this.WindowState = FormWindowState.Minimized;
        this.ShowInTaskbar = false;
      }
      else
      {
        this.WindowState = FormWindowState.Normal;
        this.ShowInTaskbar = true;
      }
    }

    private void notifyIcon_MouseMove(object sender, MouseEventArgs e)
    {
      if (!bBaloonVisible)
        notifyIcon.ShowBalloonTip(100);
    }

    private void notifyIcon_BalloonTipShown(object sender, EventArgs e)
    {
      bBaloonVisible = true;
    }

    private void notifyIcon_BalloonTipClosed(object sender, EventArgs e)
    {
      bBaloonVisible = false;
    }

    private void notifyIcon_Click(object sender, EventArgs e)
    {
      notifyIcon_DoubleClick(this, e);
    }
  }
}
